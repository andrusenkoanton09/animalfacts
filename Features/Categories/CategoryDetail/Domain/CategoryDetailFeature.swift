import ComposableArchitecture

struct CategoryDetailFeature: ReducerProtocol {
    struct State: Equatable {
        let category: Category
        var currentFactIndex = 0
        var isShareSheetPresented = false
        
        var fact: Content {
            category.content[currentFactIndex]
        }
    }

    enum Action: Equatable {
        case next
        case previous
        case setShareSheet(isPresented: Bool)
    }

    var body: some ReducerProtocolOf<Self> {
        Reduce { state, action in
            switch action {
            case let .setShareSheet(isPresented):
                state.isShareSheetPresented = isPresented
                return .none

            case .previous:
                if state.currentFactIndex == 0 {
                    state.currentFactIndex = state.category.content.count - 1
                    return .none
                }
                state.currentFactIndex -= 1
                return .none
                
            case .next:
                if state.currentFactIndex + 1 == state.category.content.count {
                    state.currentFactIndex = 0
                    return .none
                }
                state.currentFactIndex += 1
                return .none
            }
        }
    }
}
