import ComposableArchitecture
import SwiftUI

internal struct CategoryDetailView: View {
    let store: StoreOf<CategoryDetailFeature>
    
    @Environment(\.dismiss) var dismiss

    var body: some View {
        WithViewStore(self.store, observe: { $0 }) { viewStore in
            VStack {
                HStack {
                    Button {
                        dismiss()
                    } label: {
                        Image("ic_back")
                    }
                    Spacer()
                    Text(viewStore.category.title)
                        .font(Font.custom(.regularHeadline))
                    Spacer()
                    Button {
                        viewStore.send(.setShareSheet(isPresented: true))
                    } label: {
                        Image(systemName: "square.and.arrow.up")
                            .foregroundColor(Color.black)
                    }
                }
                .padding()
                .frame(maxHeight: 90)
                .background(
                    Color.background.shadow(color: Color.black.opacity(0.25), radius: 4, x: 0, y: 10)
                )
                Spacer()
                CategoryDetailCard(content: viewStore.state.fact) {
                    viewStore.send(.previous)
                } nextAction: {
                    viewStore.send(.next)
                }
                .gesture(DragGesture().onEnded { value in
                    if value.translation.width < 0 {
                        viewStore.send(.next)
                    }
                    if value.translation.width > 0 {
                        viewStore.send(.previous)
                    }
                })
                Spacer()
            }
            .navigationBarBackButtonHidden(true)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(Color.background)
            .sheet(
                isPresented: viewStore.binding(
                    get: { $0.isShareSheetPresented },
                    send: CategoryDetailFeature.Action.setShareSheet(isPresented:)
                )
            ) {
                ActivityViewController(
                    activityItems: [
                        URL(string: viewStore.state.fact.imageURL)!,
                        viewStore.state.fact.fact
                    ]
                )
            }
        }
    }
}

internal struct CategoryDetailView_Previews: PreviewProvider {
    static let category = Category(title: "Title", description: "Description", imageURL: "https://images.dog.ceo/breeds/basenji/n02110806_4150.jpg", order: 1, status: .free, content: [
        .init(fact: "1", imageURL: "https://images.dog.ceo/breeds/basenji/n02110806_4150.jpg"), .init(fact: "2", imageURL: "https://images.dog.ceo/breeds/basenji/n02110806_4150.jpg")])
    static var previews: some View {
        NavigationView {
            CategoryDetailView(store: Store(initialState: .init(category: category), reducer: { CategoryDetailFeature() }))
        }
    }
}
