import SwiftUI

internal struct CategoryDetailCard: View {
    let content: Content
    let prevAction: () -> Void
    let nextAction: () -> Void
    
    var body: some View {
        VStack(spacing: 0) {
            AsyncImage(url: URL(string: content.imageURL)!) { phase in
                switch phase {
                case .empty:
                    ProgressView()
                        .frame(maxWidth: 315, maxHeight: 235)
                case .success(let image):
                    image
                        .resizable()
                        .frame(maxWidth: 315, maxHeight: 235)
                        .scaledToFit()
                default:
                    Image(systemName: "photo")
                        .frame(maxWidth: 315, maxHeight: 235)
                }
            }
            Spacer()
                .frame(maxHeight: 17)
            Text(content.fact)
                .font(Font.custom(.regularHeadline))
            Spacer()
            HStack {
                Button {
                    prevAction()
                } label: {
                    Image("ic_back_btn")
                }
                Spacer()
                Button {
                    nextAction()
                } label: {
                    Image("ic_next_btn")
                }
            }
            .padding(.horizontal, 12)
            .padding(.vertical, 10)
        }
        .padding(10)
        .frame(width: 335, height: 445)
        .background(Color.white)
        .cornerRadius(6)
        .shadow(color: Color.black.opacity(0.3), radius: 60, x: 0, y: 20)
    }
}

internal struct CategoryDetailCard_Previews: PreviewProvider {
    static var previews: some View {
        CategoryDetailCard(
            content: .init(fact: "Hello", imageURL: "https://cataas.com/cat"),
            prevAction: {},
            nextAction: {}
        )
    }
}
