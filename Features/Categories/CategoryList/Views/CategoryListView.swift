import ComposableArchitecture
import SwiftUI

internal struct CategoryListView: View {
    let store: StoreOf<CategoryListFeature>

    var body: some View {
        NavigationView {
            WithViewStore(self.store, observe: { $0 }) { viewStore in
                ScrollView {
                    if viewStore.isLoading {
                        ProgressView()
                    } else {
                        VStack(spacing: 16) {
                            ForEach(viewStore.categories) { category in
                                NavigationLink(
                                    destination: IfLetStore(
                                        self.store.scope(
                                            state: \.selection?.value,
                                            action: CategoryListFeature.Action.details)
                                    ) {
                                        CategoryDetailView(store: $0)
                                    },
                                    tag: category.id,
                                    selection: viewStore.binding(
                                        get: \.selection?.id,
                                        send: CategoryListFeature.Action.setNavigation(selection:))
                                ) {
                                    CategoryListItemView(category: category)
                                        .onTapGesture {
                                            viewStore.send(.handteCategoryTap(category))
                                        }
                                }
                            }
                        }
                    }
                }
                .frame(maxWidth: .infinity)
                .padding(20)
                .background(Color.background)
                .onAppear {
                    viewStore.send(.fetchCategories)
                }
                .overlay {
                    if viewStore.state.isShowingAd {
                        ProgressView()
                            .accentColor(Color.green)
                            .scaleEffect(x: 2, y: 2, anchor: .center)
                            .frame(maxWidth: .infinity, maxHeight: .infinity)
                            .background(Color.gray.opacity(0.3))
                    }
                }
            }
            .alert(
                self.store.scope(state: \.alert, action: { $0 }),
                dismiss: .alertDismissed
            )
        }
    }
}

internal struct CategoryListView_Previews: PreviewProvider {
    struct MockRepository: CategoryRepository {
        func fetchCategories() async throws -> [Category] {
            [
                .init(title: "Cats", description: "Cats description", imageURL: "", order: 1, status: .free, content: [.init(fact: "Fact", imageURL: "")]),
                .init(title: "Dogs", description: "Dogs description", imageURL: "", order: 3, status: .paid, content: [.init(fact: "Fact", imageURL: "")]),
                .init(title: "Dragons", description: "Dragons description", imageURL: "", order: 2, status: .free, content: []),
            ]
        }
    }

    static var previews: some View {
        CategoryListView(
            store: Store(
                initialState: CategoryListFeature.State(),
                reducer: { CategoryListFeature(categoryRepository: MockRepository()) }
            )
        )
    }
}




