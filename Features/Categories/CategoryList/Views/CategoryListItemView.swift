import SwiftUI

internal struct CategoryListItemView: View {
    let category: Category

    var body: some View {
        ZStack {
            HStack(spacing: 15) {
                AsyncImage(url: .init(string: category.imageURL)!) { phase in
                    switch phase {
                    case .empty:
                        ProgressView()
                            .frame(maxWidth: 120, maxHeight: 90)
                    case .success(let image):
                        image
                            .resizable()
                            .frame(maxWidth: 120, maxHeight: 90)
                            .scaledToFit()
                    default:
                        Image(systemName: "photo")
                    }
                }
                VStack(alignment: .leading, spacing: 0) {
                    Text(category.title)
                        .padding(.top, 5)
                        .font(Font.custom(.regularTitle))
                    Text(category.description)
                        .font(Font.custom(.regularBody))
                        .foregroundColor(Color.black.opacity(0.5))
                    Spacer()
                    if category.status == .paid && !category.content.isEmpty {
                        HStack {
                            Image("ic_lock")
                            Text("Premium")
                                .foregroundColor(Color.accent)
                                .font(Font.custom(.regularTitle))
                        }
                    }
                }
                Spacer()
            }
            .padding(5)
            if category.content.isEmpty {
                HStack {
                    Spacer()
                    Image("coming_soon")
                }
                .background(Color.black.opacity(0.6))
            }
        }
        .frame(maxWidth: .infinity, maxHeight: 100)
        .background(Color.white)
        .cornerRadius(6)
        .shadow(color: Color.black.opacity(0.2), radius: 2, x: 0, y: 2)
    }
}

internal struct CategoryListItemView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CategoryListItemView(category: .init(title: "Title", description: "Description", imageURL: "https://images.dog.ceo/breeds/basenji/n02110806_4150.jpg", order: 1, status: .free, content: [.init(fact: "", imageURL: "")]))
                .previewDisplayName("Free")
            CategoryListItemView(category: .init(title: "Title", description: "Description", imageURL: "https://images.dog.ceo/breeds/basenji/n02110806_4150.jpg", order: 1, status: .paid, content: [.init(fact: "", imageURL: "")]))
                .previewDisplayName("Paid")
            CategoryListItemView(category: .init(title: "Title", description: "Description", imageURL: "https://images.dog.ceo/breeds/basenji/n02110806_4150.jpg", order: 1, status: .paid, content: []))
                .previewDisplayName("Coming Soon")
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding()
        .background(Color.background)
    }
}
