import Foundation

protocol CategoryRepository {
    func fetchCategories() async throws -> [Category]
}
