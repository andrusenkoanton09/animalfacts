import Foundation

struct Content: Identifiable, Equatable {
    let id = UUID()
    let fact: String
    let imageURL: String
}
