import Foundation

struct Category: Identifiable, Equatable {
    let id = UUID()
    let title: String
    let description: String
    let imageURL: String
    let order: Int
    let status: CategoryStatus
    let content: [Content]
}
