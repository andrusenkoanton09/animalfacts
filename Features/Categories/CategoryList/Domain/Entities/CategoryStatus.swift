import Foundation

enum CategoryStatus: String, Equatable {
    case free
    case paid
}
