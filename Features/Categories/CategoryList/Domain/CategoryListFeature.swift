import ComposableArchitecture
import Foundation

struct CategoryListFeature: ReducerProtocol {
    struct State: Equatable {
        var alert: AlertState<Action>?
        var isLoading = false
        var categories: [Category] = []
        var isShowingAd = false
        var selection: Identified<UUID, CategoryDetailFeature.State>?
    }

    enum Action: Equatable {
        case fetchCategories
        case handleCategoriesResponse([Category])
        case handteCategoryTap(Category)
        case alertDismissed
        case displayAd(selection: UUID)
        case closeAd(selection: UUID)
        case setNavigation(selection: UUID?)
        case details(CategoryDetailFeature.Action)
    }

    let categoryRepository: CategoryRepository

    var body: some ReducerProtocolOf<Self> {
        Reduce { state, action in
            switch action {
            case .details:
                return .none

            case .fetchCategories:
                state.isLoading = true
                state.categories = []
                return .run { send in
                    var response = try await categoryRepository.fetchCategories()
                    response.sort(by: { $0.order < $1.order })
                    await send(.handleCategoriesResponse(response))
                }

            case let .handleCategoriesResponse(categories):
                state.isLoading = false
                state.categories = categories
                return .none

            case let .handteCategoryTap(category):
                if category.content.isEmpty {
                    state.alert = AlertState {
                        TextState("Cooming Soon!")
                    } actions: {
                        ButtonState(role: .cancel) {
                            TextState("Ok")
                        }
                    }
                    return .none
                }
                if category.status == .paid {
                    state.alert = AlertState {
                        TextState("Watch Ad to continue")
                    } actions: {
                        ButtonState(action: .displayAd(selection: category.id)) {
                            TextState("Show Ad")
                        }
                        ButtonState(role: .cancel) {
                            TextState("Cancel")
                        }
                    }
                    return .none
                }
                return .send(.setNavigation(selection: category.id))

            case let .displayAd(id):
                state.isShowingAd = true
                return .run { send in
                    try await Task.sleep(nanoseconds: 2 * 1_000_000_000)
                    await send(.closeAd(selection: id))
                }

            case let .closeAd(id):
                state.isShowingAd = false
                return .send(.setNavigation(selection: id))

            case .alertDismissed:
                state.alert = nil
                return .none
                
            case let .setNavigation(selection: .some(id)):
                let category = state.categories.first(where: { $0.id == id})!
                state.selection = Identified(
                    CategoryDetailFeature.State(category: category),
                    id: id
                )
                return .none
                
            case .setNavigation(selection: .none):
                state.selection = nil
                return.none
            }
        }
        .ifLet(\.selection, action: /Action.details) {
            Scope(state: \Identified<UUID, CategoryDetailFeature.State>.value, action: /.self) {
                CategoryDetailFeature()
            }
        }
    }
}
