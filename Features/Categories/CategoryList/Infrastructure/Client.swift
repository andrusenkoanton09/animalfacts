import Foundation

struct Client {
    func getCategories() async throws -> [CategoryResponse] {
        let url = URL(string: "https://raw.githubusercontent.com/AppSci/promova-test-task-iOS/main/animals.json")!
        let (data, _) = try await URLSession.shared.data(from: url)
        let response = try JSONDecoder().decode([CategoryResponse].self, from: data)
        return response
    }
}
