import Foundation

struct CategoryRepositoryImpl: CategoryRepository {
    let client: Client

    func fetchCategories() async throws -> [Category] {
        let response = try await client.getCategories()
        let categories = response.map { $0.toEntity() }
        return categories
    }
}
