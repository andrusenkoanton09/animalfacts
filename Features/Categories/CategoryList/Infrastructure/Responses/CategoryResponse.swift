import Foundation

struct CategoryResponse: Codable {
    let title: String
    let description: String
    let image: String
    let order: Int
    let status: String
    let content: [ContentResponse]?

    func toEntity() -> Category {
        Category(
            title: title,
            description: description,
            imageURL: image,
            order: order,
            status: CategoryStatus(rawValue: status)!,
            content: content?.map { $0.toEntity() } ?? []
        )
    }
}
