import Foundation

struct ContentResponse: Codable {
    let fact: String
    let image: String

    func toEntity() -> Content {
        Content(
            fact: fact,
            imageURL: image
        )
    }
}
