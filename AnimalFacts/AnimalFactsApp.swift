import SwiftUI
import ComposableArchitecture

@main
struct AnimalFactsApp: App {
    var body: some Scene {
        WindowGroup {
            CategoryListView(store: Store(initialState: CategoryListFeature.State()) {
                CategoryListFeature(categoryRepository: CategoryRepositoryImpl(client: Client()))
              })
        }
    }
}
