import SwiftUI

extension Font {
    static func custom(_ style: CustomFontStyle) -> Font {
        Font.custom("Basic-Regular", size: style.fontSize)
    }
    
    enum CustomFontStyle {
        case regularTitle
        case regularBody
        case regularHeadline
        
        var fontSize: CGFloat {
            switch self {
            case .regularTitle: return 16
            case .regularBody: return 12
            case .regularHeadline: return 18
            }
        }
    }
}
