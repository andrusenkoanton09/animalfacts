import SwiftUI

extension Color {
    static let background = Color("background")
    static let accent = Color("accent")
}
